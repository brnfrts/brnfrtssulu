# BrnfrtsSulu

## Description
This repository contains various addons for the Sulu CMS.

## Installation
You can install this package via Composer. 

- Run the following command in your terminal:

```bash
composer require brnfrts/brnfrtssulu
```

- Add this to your services.yaml file:

```yaml
#BrnfrtsSulu import
imports:
- { resource: '../src/BrnfrtsSulu/Resources/config/services.yaml' }
```

## Usage
Available Commands:

#### Rename Media Files Command
This command renames media files based on their titles in the Sulu CMS.

```bash
php bin/console brnfrts:rename-media-files
```
#### Regenerate Media Images Command
This command regenerates images for all media in the Sulu CMS. If you used above command to rename media files, all possible image sizes can be generated automatically with this command.

```bash
php bin/console brnfrts:regenerate-media-images
php -d memory_limit=5G bin/console brnfrts:regenerate-media-images #if you hit a memory limit
```

## Caution
As always: Make a backup first of your database and media files before using these commands.
