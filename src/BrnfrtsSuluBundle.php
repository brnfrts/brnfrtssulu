<?php

namespace Brnfrts\BrnfrtsSulu;

use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class BrnfrtsSuluBundle extends AbstractBundle
{
    private $files = [
        [
            'fileName' => 'services.yaml',
            'targetDir' => '/packages/brnfrts/brnfrtssulu',
        ],
    ];

    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $projectDir = $container->getParameter('kernel.project_dir');
        $configDir = $projectDir . '/config';

        $filesystem = new Filesystem();
        $filesystem->mkdir($configDir);

        foreach ($this->files as $file) {
            $source = __DIR__ . '/Resources/config/' . $file['fileName'];
            $target = $configDir . $file['targetDir'] . '/' . $file['fileName'];

            if (!$filesystem->exists($target)) {
                $filesystem->copy($source, $target, true);
            }
        }
    }

    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $projectDir = $builder->getParameter('kernel.project_dir');
        $configDir = $projectDir . '/config';

        foreach ($this->files as $file) {
            $locator = new FileLocator($configDir . $file['targetDir']);
            $path = $locator->locate($file['fileName'], null, true);

            $container->import($path);
        }
    }
}
