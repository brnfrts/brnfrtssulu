<?php

namespace Brnfrts\BrnfrtsSulu\Command;

use Sulu\Bundle\MediaBundle\Entity\MediaRepositoryInterface;
use Sulu\Bundle\MediaBundle\Media\Exception\FileVersionNotFoundException;
use Sulu\Bundle\MediaBundle\Media\Exception\MediaNotFoundException;
use Sulu\Bundle\MediaBundle\Media\FormatManager\FormatManagerInterface;
use Sulu\Bundle\MediaBundle\Media\FormatOptions\FormatOptionsManagerInterface;
use Sulu\Bundle\MediaBundle\Media\Manager\MediaManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Contracts\Service\Attribute\Required;
use Throwable;

#[AsCommand(
    name: 'brnfrts:regenerate-media-images',
    description: 'Regenerates images for all media in Sulu CMS',
)]
class RegenerateMediaImagesCommand extends Command
{
    private $mediaManager;
    private $formatManager;
    private $formatOptionsManager;

    #[Required]
    public function __construct(MediaManagerInterface $mediaManager, FormatManagerInterface $formatManager, FormatOptionsManagerInterface $formatOptionsManager)
    {
        parent::__construct();

        $this->mediaManager = $mediaManager;
        $this->formatManager = $formatManager;
        $this->formatOptionsManager = $formatOptionsManager;
    }

    protected function configure(): void
    {
        $this->setDescription('Regenerates images for all media in Sulu CMS.');
    }
    /**
     * @throws MediaNotFoundException
     * @throws FileVersionNotFoundException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Regenerating Media Images');

        $mediaItems = $this->mediaManager->get('nl');
        $totalMedia = count($mediaItems);

        if ($totalMedia === 0) {
            $io->warning('No media found to regenerate.');
            return Command::SUCCESS;
        }

        $progressBar = $io->createProgressBar($totalMedia);
        $progressBar->start();

        $formats = $this->formatManager->getFormatDefinitions();

        foreach ($mediaItems as $media) {
            $file = $media->getFile();
            $fileVersions = $file->getFileVersions();
            $formats = $this->formatManager->getFormatDefinitions();

            foreach ($fileVersions as $fileVersion) {
                $meta = $fileVersion->getMeta()->first();
                foreach ($formats as $formatKey => $options) {
                    try {
                        $this->formatManager->returnImage(
                            $media->getId(),
                            $formatKey,
                            $fileVersion->getName()
                        );
                    } catch (Throwable $e) {
                        $io->error(sprintf(
                            'Failed to regenerate image for media ID %s, format %s: %s',
                            $media->getId(),
                            $formatKey,
                            $e->getMessage()
                        ));
                    }
                }
            }

            $progressBar->advance();
        }

        $progressBar->finish();
        $io->success('Finished regenerating images for all media.');

        return Command::SUCCESS;
    }
}
