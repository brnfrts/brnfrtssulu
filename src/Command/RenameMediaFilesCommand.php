<?php

namespace Brnfrts\BrnfrtsSulu\Command;

use Doctrine\ORM\EntityManagerInterface;
use Sulu\Bundle\MediaBundle\Media\Manager\MediaManagerInterface;
use Sulu\Bundle\MediaBundle\Entity\FileVersionMeta;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Contracts\Service\Attribute\Required;

#[AsCommand(
    name: 'brnfrts:rename-media-files',
    description: 'Rename media files based on their title in Sulu CMS.',
)]
class RenameMediaFilesCommand extends Command
{
    private $mediaManager;
    private $entityManager;

    public function __construct(MediaManagerInterface $mediaManager, EntityManagerInterface $entityManager)
    {
        parent::__construct();

        $this->mediaManager = $mediaManager;
        $this->entityManager = $entityManager;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Renames media files based on their titles in Sulu CMS.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Renaming media files based on their titles in Sulu CMS.');

        // Retrieve media items from the media manager
        $mediaItems = $this->mediaManager->get('nl');
        $count = 0;

        foreach ($mediaItems as $media) {
            $file = $media->getFile();
            $fileVersions = $file->getFileVersions();
            foreach ($fileVersions as $fileVersion) {
                $meta = $fileVersion->getMeta()->first();
                if ($meta instanceof FileVersionMeta) {
                    $title = $meta->getTitle();
                    $filename = $fileVersion->getName();
                    $newFilename = $this->generateFilenameFromTitle($title, $filename);

                    $fileVersion->setName($newFilename);
                    $this->entityManager->flush();

                    $io->success(sprintf('Successfully renamed %s to %s', $filename, $newFilename));
                    $count++;
                }
            }
        }

        $io->success($count . ' media files have been renamed.');

        return Command::SUCCESS;
    }

    private function generateFilenameFromTitle(string $title, string $currentFilename): string
    {
        $extension = pathinfo($currentFilename, PATHINFO_EXTENSION);

        return $this->slugify($title) . $extension;
    }

    private function slugify(string $title): string
    {
        return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $title)));
    }
}
