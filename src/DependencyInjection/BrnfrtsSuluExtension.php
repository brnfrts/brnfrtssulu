<?php

namespace Brnfrts\BrnfrtsSulu\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class BrnfrtsSuluExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $projectDir = $container->getParameter('kernel.project_dir');
        $configDir = $projectDir . '/config/brnfrts/brnfrtssulu';

        $loader = new YamlFileLoader(
            $container,
            new FileLocator($configDir)
        );

        $loader->load('services.yaml');
    }
}
